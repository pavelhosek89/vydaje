#!/usr/bin/python

from gi.repository import Gtk, GdkPixbuf, Gdk
import os, sys
from pprint import pprint

class Calendar():
	def __init__(self, parent):

		self.date = 0
		
		self.dialog = Gtk.Dialog(title="Calendar", parent=parent, buttons=("_OK", Gtk.ResponseType.OK, "_Cancel", Gtk.ResponseType.CANCEL))
		self.dialog.set_resizable(False)
		self.dialog.set_size_request(300,230)

		self.dialog.connect("key-press-event",self._key_press_event)
		
		self.calendar = Gtk.Calendar()
		content = self.dialog.get_content_area()
		content.add(self.calendar)
		self.today = self.calendar.get_date()
		self.dialog.show_all()
		
		self.response = self.dialog.run()

		if self.response == Gtk.ResponseType.OK:
			self.date = self.calendar.get_date()
#			print(date)
#			print("OK button clicked")
#		elif self.response == Gtk.ResponseType.CANCEL:
#			print("Cancel button clicked")
#		else:
#			print("Dialog closed")
		self.dialog.destroy()

	def get_date(self):
		return self.date

	def _key_press_event(self,widget,event):
		keyname = Gdk.keyval_name(event.keyval)
		ctrl = event.state & Gdk.ModifierType.CONTROL_MASK
		if ctrl:
			if keyname == 'Return':
				self.date = self.calendar.get_date()
				self.dialog.destroy()