#!/usr/bin/python

from sqlalchemy import Column, Date, Float, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
 
Base = declarative_base()
 
class Purchase(Base):
    __tablename__ = 'purchase'
    # Here we define columns for the table person
    # Notice that each column is also a normal Python instance attribute.
    id = Column(Integer, primary_key=True)
    date = Column(Date, nullable=False)
    name = Column(String(250))

    def __repr__(self):
        return "<Purchase(id='%s', date='%s', name='%s')>" % (self.id, self.date, self.name)
 
class Item(Base):
    __tablename__ = 'item'
    # Here we define columns for the table address.
    # Notice that each column is also a normal Python instance attribute.
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    amount = Column(Integer, nullable=False)
    unit = Column(String(250), nullable=False)
    price = Column(Float, nullable=False)
    usefulness = Column(String(250), nullable=False)
    category = Column(String(250), nullable=False)
    purchase_id = Column(Integer, ForeignKey('purchase.id'))
    purchase = relationship(Purchase)

    def __repr__(self):
        return "<Purchase(id='%s', name='%s', amount='%s', unit='%s', price='%s', usefulness='%s', category='%s', purchase_id='%s')>" % (
                self.id, self.name, self.amount, self.unit, self.price, self.usefulness, self.category, self.purchase_id)

class Combobox(Base):
    __tablename__ = 'combobox'
    # Here we define columns for the table person
    # Notice that each column is also a normal Python instance attribute.
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    type = Column(String(250), nullable=False)

    def __repr__(self):
        return "<Purchase(id='%s', name='%s', type='%s')>" % (self.id, self.name, self.type)
 
# Create an engine that stores data in the local directory's
# sqlalchemy_example.db file.
engine = create_engine('sqlite:///database_v1.sqlite')
 
# Create all tables in the engine. This is equivalent to "Create Table"
# statements in raw SQL.
Base.metadata.create_all(engine)

