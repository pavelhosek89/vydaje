#!/usr/bin/python

import os, sys
from datetime import date
from sqlalchemy import create_engine, distinct, func
from sqlalchemy.orm import sessionmaker
 
from sqlalchemy_declarative import Base, Purchase, Item, Combobox

class DatabaseService():

	def __init__(self):
		if os.path.isfile('database_v1.sqlite'):
			engine = create_engine('sqlite:///database_v1.sqlite')
			Base.metadata.bind = engine
			DBSession = sessionmaker(bind=engine)
			self.session = DBSession()
		else:
			print('ERROR with db file!!!')
			sys.exit(1)


	def print_users(self):
		result = self.session.query(Combobox).filter(Combobox.type == "category").all()
		for row in result:
			print(row.name, row.type)


	def insert_item(self, data = []):
		time = data[7].split('-')
		d = date(int(time[0]), int(time[1]), int(time[2]))
		last_purchase = self.get_last_purchase()
		if str(last_purchase.name) == str(data[6]) and str(last_purchase.date) == str(data[7]):
			item = Item(name=data[0], amount=data[1], unit=data[2], price=data[3], usefulness=data[4], category=data[5], purchase_id=(last_purchase.id))
			self.session.add(item)
		else:
			purchase = Purchase(name=data[6], date=d)
			self.session.add(purchase)
			item = Item(name=data[0], amount=data[1], unit=data[2], price=data[3], usefulness=data[4], category=data[5], purchase_id=(last_purchase.id+1))
			self.session.add(item)
		if self.get_vendor_name(data[6]) == []:
			combobox = Combobox(name=data[6], type='vendor')
			self.session.add(combobox)		
		self.session.commit()


	def get_all_category_name(self):
		return self.session.query(Combobox.name).filter(Combobox.type == "category").order_by(Combobox.name).all()


	def get_all_vendor_name(self):
		return self.session.query(Combobox.name).filter(Combobox.type == "vendor").order_by(Combobox.name).all()


	def get_vendor_name(self, name):
		return self.session.query(Combobox.name).filter(Combobox.type == "vendor").filter(Combobox.name == name).all()


	def get_all_usefulness_name(self):
		return self.session.query(Combobox.name).filter(Combobox.type == "usefulness").order_by(Combobox.name).all()


	def get_all_item_name(self):
		data = self.session.query(distinct(Item.name)).order_by(Item.name).all()
		return data


	def get_all_purchase_name(self):
		return self.session.query(distinct(Purchase.name)).order_by(Purchase.name).all()


	def get_last_item(self, name):
		return self.session.query(Item).filter(Item.name == name).order_by(Item.id.desc()).first()


	def get_last_purchase(self):
		return self.session.query(Purchase).order_by(Purchase.id.desc()).first()


	def get_month_item(self, month):
		return self.session.query(Item.name, Item.amount, Item.unit, Item.price, Item.usefulness, Item.category, Purchase.name, Purchase.date).join(Purchase, Item.purchase_id==Purchase.id).filter(Purchase.date >= month).order_by(Purchase.date).all()


	def get_month_sum(self, date):
		return self.session.query(func.sum(Item.price).label("sum"),func.count(Item.id).label("count")).join(Purchase, Item.purchase_id==Purchase.id).filter(Purchase.date >= date).all()


	def get_date_item(self, date1, date2, item_name, purchase_name, category):
		return self.session.query(Item.name, Item.amount, Item.unit, Item.price, Item.usefulness, Item.category, Purchase.name, Purchase.date).join(Purchase, Item.purchase_id==Purchase.id).filter(Purchase.date >= date1).filter(Purchase.date <= date2).filter(Item.name.like("%"+item_name+"%")).filter(Purchase.name.like("%"+purchase_name+"%")).filter(Item.category.like("%"+category+"%")).order_by(Purchase.date).all()


	def get_date_item_2(self, date1, date2, item_name, purchase_name, category):
		return self.session.query(Item.name, func.sum(func.round(Item.amount)), Item.unit, func.sum(Item.price), Item.usefulness, Item.category, Purchase.name, Purchase.date).join(Purchase, Item.purchase_id==Purchase.id).filter(Purchase.date >= date1).filter(Purchase.date <= date2).filter(Item.name.like("%"+item_name+"%")).filter(Purchase.name.like("%"+purchase_name+"%")).filter(Item.category.like("%"+category+"%")).group_by(Item.name).order_by(Purchase.date).all()


	def get_date_sum(self, date1, date2, item_name, purchase_name, category):
		return self.session.query(func.sum(Item.price).label("sum"),func.count(Item.id).label("count")).join(Purchase, Item.purchase_id==Purchase.id).filter(Purchase.date >= date1).filter(Purchase.date <= date2).filter(Item.name.like("%"+item_name+"%")).filter(Purchase.name.like("%"+purchase_name+"%")).filter(Item.category.like("%"+category+"%")).all()


	def get_statistic_data(self, date1, date2, category):
		return self.session.query(func.sum(Item.price).label("sum"),func.count(Item.id).label("count")).join(Purchase, Item.purchase_id==Purchase.id).filter(Purchase.date >= date1).filter(Purchase.date <= date2).filter(Item.category == category).all()
