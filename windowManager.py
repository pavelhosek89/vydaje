#!/usr/bin/python

from gi.repository import Gtk, GdkPixbuf, Gdk, Gio
from dialogManager import Calendar
from datetime import date
from databaseService import DatabaseService
from pprint import pprint

UI_FILE = "testicek.ui"

class RendererWindow():

	def __init__(self):

		builder = Gtk.Builder()
		builder.add_from_file(UI_FILE)
		builder.connect_signals(self)

		self.db = DatabaseService()
		
		self.window = builder.get_object('ShopList')
		
		#Klávesové zkratky
		self.window.connect("key-press-event",self._key_press_event)
		self.fullscreen=False

		self.liststore = builder.get_object('liststore')
		self.liststore1 = builder.get_object('liststore1')

		liststore_jednotka = Gtk.ListStore(str)
		list_jednotka = ["gram", "mililitr", "kus", "svazek"]
		for item in list_jednotka:
			liststore_jednotka.append([item])
			
		treeview1 = builder.get_object('treeview1')
		treeview1.set_model(model=self.liststore)
		treeview2 = builder.get_object('treeview2')
		treeview2.set_model(model=self.liststore1)

		self.label28 = builder.get_object('label28')
		self.label16 = builder.get_object('label16')
		self.label17 = builder.get_object('label17')
		self.label22 = builder.get_object('label22')
		self.label23 = builder.get_object('label23')
		self.label38 = builder.get_object('label38')
		self.label39 = builder.get_object('label39')
		self.label44 = builder.get_object('label44')
		self.label46 = builder.get_object('label46')
		self.label48 = builder.get_object('label48')
		self.label63 = builder.get_object('label63')
		self.entry1 = builder.get_object('entry1')
		self.entry2 = builder.get_object('entry2')
		self.entry3 = builder.get_object('entry3')
		self.entry4 = builder.get_object('entry4')
		self.entry5 = builder.get_object('entry5')
		self.entry6 = builder.get_object('entry6')
		self.entry7 = builder.get_object('entry7')
		self.comboboxtext1 = builder.get_object('comboboxtext1')
		self.comboboxtext3 = builder.get_object('comboboxtext3')
		self.comboboxtext4 = builder.get_object('comboboxtext4')
		self.checkbutton1 = builder.get_object('checkbutton1')

		#setting for treeview1
		renderer_polozka = Gtk.CellRendererText()
		renderer_polozka.set_property("editable", True)
		renderer_polozka.connect("edited", self.polozka_edited)
		renderer_mnozstvi = Gtk.CellRendererText()
		renderer_jednotka = Gtk.CellRendererCombo()
		renderer_jednotka.set_property("editable", True)
		renderer_jednotka.set_property("model", liststore_jednotka)
		renderer_jednotka.set_property("text-column", 0)
		renderer_jednotka.set_property("has-entry", False)
		renderer_jednotka.connect("edited", self.jednotka_edited)
		renderer_cena = Gtk.CellRendererText()
		renderer_potrebnost = Gtk.CellRendererText()
		renderer_ucel = Gtk.CellRendererText()
		renderer_prodejce = Gtk.CellRendererText()
		renderer_datum = Gtk.CellRendererText()
		polozka = builder.get_object('polozka')
		polozka.pack_start(renderer_polozka, True)
		polozka.add_attribute(renderer_polozka, "text", 0);
		polozka.set_sort_column_id(0)
		mnozstvi = builder.get_object('mnozstvi')
		mnozstvi.pack_start(renderer_mnozstvi, True)
		mnozstvi.add_attribute(renderer_mnozstvi, "text", 1);
		jednotka = builder.get_object('jednotka')
		jednotka.pack_start(renderer_jednotka, True)
		jednotka.add_attribute(renderer_jednotka, "text", 2);
		cena = builder.get_object('cena')
		cena.pack_start(renderer_cena, True)
		cena.add_attribute(renderer_cena, "text", 3);
		cena.set_sort_column_id(3)
		potrebnost = builder.get_object('potrebnost')
		potrebnost.pack_start(renderer_potrebnost, True)
		potrebnost.add_attribute(renderer_potrebnost, "text", 4);
		potrebnost.set_sort_column_id(4)
		ucel = builder.get_object('ucel')
		ucel.pack_start(renderer_ucel, True)
		ucel.add_attribute(renderer_ucel, "text", 5);
		ucel.set_sort_column_id(5)
		prodejce = builder.get_object('prodejce')
		prodejce.pack_start(renderer_prodejce, True)
		prodejce.add_attribute(renderer_prodejce, "text", 6);
		prodejce.set_sort_column_id(6)
		datum = builder.get_object('datum')
		datum.pack_start(renderer_datum, True)
		datum.add_attribute(renderer_datum, "text", 7);
		datum.set_sort_column_id(7)

		#setting for treeview2
		renderer_polozka1 = Gtk.CellRendererText()
		renderer_mnozstvi1 = Gtk.CellRendererText()
		renderer_jednotka1 = Gtk.CellRendererCombo()
		renderer_cena1 = Gtk.CellRendererText()
		renderer_potrebnost1 = Gtk.CellRendererText()
		renderer_ucel1 = Gtk.CellRendererText()
		renderer_prodejce1 = Gtk.CellRendererText()
		renderer_datum1 = Gtk.CellRendererText()
		polozka1 = builder.get_object('polozka1')
		polozka1.pack_start(renderer_polozka, True)
		polozka1.add_attribute(renderer_polozka, "text", 0);
		polozka1.set_sort_column_id(0)
		mnozstvi1 = builder.get_object('mnozstvi1')
		mnozstvi1.pack_start(renderer_mnozstvi, True)
		mnozstvi1.add_attribute(renderer_mnozstvi, "text", 1);
		jednotka1 = builder.get_object('jednotka1')
		jednotka1.pack_start(renderer_jednotka, True)
		jednotka1.add_attribute(renderer_jednotka, "text", 2);
		cena1 = builder.get_object('cena1')
		cena1.pack_start(renderer_cena, True)
		cena1.add_attribute(renderer_cena, "text", 3);
		cena1.set_sort_column_id(3)
		potrebnost1 = builder.get_object('potrebnost1')
		potrebnost1.pack_start(renderer_potrebnost, True)
		potrebnost1.add_attribute(renderer_potrebnost, "text", 4);
		potrebnost1.set_sort_column_id(4)
		ucel1 = builder.get_object('ucel1')
		ucel1.pack_start(renderer_ucel, True)
		ucel1.add_attribute(renderer_ucel, "text", 5);
		ucel1.set_sort_column_id(5)
		prodejce1 = builder.get_object('prodejce1')
		prodejce1.pack_start(renderer_prodejce, True)
		prodejce1.add_attribute(renderer_prodejce, "text", 6);
		prodejce1.set_sort_column_id(6)
		datum1 = builder.get_object('datum1')
		datum1.pack_start(renderer_datum, True)
		datum1.add_attribute(renderer_datum, "text", 7);
		datum1.set_sort_column_id(7)

		#load data to comboboxes
		usefulness = self.db.get_all_usefulness_name()
		for item in usefulness:
			self.comboboxtext3.append_text(item[0])
		category = self.db.get_all_category_name()
		for item in category:
			self.comboboxtext4.append_text(item[0])
			self.comboboxtext1.append_text(item[0])
		self.comboboxtext1.append_text('')
		self.comboboxtext3.set_active(0)
		self.comboboxtext4.set_active(0)
		del usefulness, category

		#load data to EntryCompletion entry1
		completionEntry1 = Gtk.EntryCompletion()
		self.entry1Completion = Gtk.ListStore(str)
		item_nameEntry1 = self.db.get_all_item_name()
		for item in item_nameEntry1:
			self.entry1Completion.append([item[0]])
		completionEntry1.set_model(self.entry1Completion)
		self.entry1.set_completion(completionEntry1)
		completionEntry1.set_text_column(0)
		completionEntry1.connect('match-selected', self.match_cb)
		self.entry1.connect('activate', self.activate_cb)
		del item_nameEntry1

		#load data to EntryCompletion entry4
		completionEntry4 = Gtk.EntryCompletion()
		self.entry4Completion = Gtk.ListStore(str)
		item_nameEntry4 = self.db.get_all_purchase_name()
		for item in item_nameEntry4:
			self.entry4Completion.append([item[0]])
		completionEntry4.set_model(self.entry4Completion)
		self.entry4.set_completion(completionEntry4)
		completionEntry4.set_text_column(0)
		completionEntry4.connect('match-selected', self.match_cb_Entry4)
		self.entry4.connect('activate', self.activate_cb_Entry4)
		del item_nameEntry4

		#load data to EntryCompletion entry6
		completionEntry6 = Gtk.EntryCompletion()
		self.entry6Completion = Gtk.ListStore(str)
		item_nameEntry6 = self.db.get_all_item_name()
		for item in item_nameEntry6:
			self.entry6Completion.append([item[0]])
		completionEntry6.set_model(self.entry6Completion)
		self.entry6.set_completion(completionEntry6)
		completionEntry6.set_text_column(0)
		completionEntry6.connect('match-selected', self.match_cb_Entry6)
		self.entry6.connect('activate', self.activate_cb_Entry6)
		del item_nameEntry6

		#load data to EntryCompletion entry7
		completionEntry7 = Gtk.EntryCompletion()
		self.entry7Completion = Gtk.ListStore(str)
		item_nameEntry7 = self.db.get_all_purchase_name()
		for item in item_nameEntry7:
			self.entry7Completion.append([item[0]])
		completionEntry7.set_model(self.entry7Completion)
		self.entry7.set_completion(completionEntry7)
		completionEntry7.set_text_column(0)
		completionEntry7.connect('match-selected', self.match_cb_Entry7)
		self.entry7.connect('activate', self.activate_cb_Entry7)

		del item_nameEntry7

		#today and this_month, set label28
		today = date.today()
		year = today.year
		if today.month >= 10:
			month = today.month
		else:
			month = '0'+str(today.month)
		this_month = str(year)+'-'+str(month)+'-01'
		self.label28.set_text(str(today))
		self.label16.set_text(str(today))
		self.label17.set_text(str(today))
		self.label38.set_text(str(today))
		self.label39.set_text(str(today))

		#load data for month to liststore1
		data_for_month = self.db.get_month_item(str(this_month))
		for item in data_for_month:
			if item[1] == '':
				item = tuple([item[0]]) + (1,) + tuple(['ks']) + tuple([str(item[3])]) + item[4:7] + tuple([str(item[7])])
				
			else:
				item = item[:3] + tuple([str(item[3])]) + item[4:7] + tuple([str(item[7])])
			self.liststore1.append(item)
		sum_for_month = self.db.get_month_sum(str(this_month))
		if sum_for_month[0][0] == None:
			self.label44.set_text("0" +" Kč ")
		else:
			self.label44.set_text(str(int(sum_for_month[0][0]))+" Kč ")
		self.label46.set_text(str(sum_for_month[0][1]))
		del data_for_month
		del sum_for_month

	def match_cb(self, completion, model, iter):
		item_data = self.db.get_last_item(model[iter][0])
		self.entry2.set_text(str(item_data.amount))
		self.entry3.set_text(str(item_data.price))
		self.entry5.set_text(str(item_data.unit))
		return

	def activate_cb(self, entry):
		text = entry.get_text()
		if text:
			if text not in [row[0] for row in self.entry1Completion]:
				self.entry1Completion.append([text])
				entry.set_text('')

	def match_cb_Entry4(self, completion, model, iter):
		return

	def activate_cb_Entry4(self, entry):
		text = entry.get_text()
		if text:
			if text not in [row[0] for row in self.entry4Completion]:
				self.entry4Completion.append([text])
				entry.set_text('')

	def match_cb_Entry6(self, completion, model, iter):
		return

	def activate_cb_Entry6(self, entry):
		text = entry.get_text()
		if text:
			if text not in [row[0] for row in self.entry6Completion]:
				self.entry6Completion.append([text])
				entry.set_text('')

	def match_cb_Entry7(self, completion, model, iter):
		return

	def activate_cb_Entry7(self, entry):
		text = entry.get_text()
		if text:
			if text not in [row[0] for row in self.entry7Completion]:
				self.entry7Completion.append([text])
				entry.set_text('')

	def on_window_destroy(self, window):
		Gtk.main_quit()

	def clicked_on_button2(self, button2):
		self.label28.set_text(self.get_date(self.label28.get_text()))
	def clicked_on_button3(self, button3):
		self.label16.set_text(self.get_date(self.label16.get_text()))
	def clicked_on_button4(self, button4):
		self.label17.set_text(self.get_date(self.label17.get_text()))
	def clicked_on_button5(self, button5):
		self.label38.set_text(self.get_date(self.label38.get_text()))
	def clicked_on_button6(self, button6):
		self.label39.set_text(self.get_date(self.label39.get_text()))

	# add data to liststore and insert to DB
	def clicked_on_button1(self, button1):
		self.add_item()
		
	def add_item(self):
		price = self.entry3.get_text()
		if "," in price:
			price = price.replace(",", ".")
		data = [self.entry1.get_text(), # name
				self.entry2.get_text(), # amount
				self.entry5.get_text(), # unit
				price, # price
				self.comboboxtext3.get_active_text(), # usefulness
				self.comboboxtext4.get_active_text(), # category
				#, # comment
				self.entry4.get_text(), # purchase(name)
				self.label28.get_text()] # date
		self.liststore.append(data)
		self.db.insert_item(data)

	# add data to liststore1
	def clicked_on_button8(self, button8):
		date1 = self.label16.get_text()
		date2 = self.label17.get_text()
		item_name = self.entry6.get_text()
		purchase_name = self.entry7.get_text()
		category = self.comboboxtext1.get_active_text()
		self.liststore1.clear()
		if category == None:
			category = ""
		if self.checkbutton1.get_active() == True:
			data_for_date = self.db.get_date_item_2(date1,date2,item_name,purchase_name,category)
		else:
			data_for_date = self.db.get_date_item(date1,date2,item_name,purchase_name,category)
		for item in data_for_date:
			item = item[:3] + tuple([str(round(item[3],2))]) + item[4:7] + tuple([str(item[7])])
			self.liststore1.append(item)
		sum_for_date = self.db.get_date_sum(date1,date2,item_name,purchase_name,category)
		if sum_for_date[0][0] == None:
			self.label44.set_text("0"+" Kč ")
		else:
			self.label44.set_text(str(int(sum_for_date[0][0]))+" Kč ")
		self.label46.set_text(str(sum_for_date[0][1]))
		del data_for_date
		del sum_for_date

	# data for statistic
	def clicked_on_button7(self, button7):
		statistic_price = ""
		statistic_count = ""
		statistic_sum_count = 0;
		date1 = self.label38.get_text()
		date2 = self.label39.get_text()
		category = self.db.get_all_category_name()
		for item in category:
			data = self.db.get_statistic_data(date1,date2,item[0])
			if data[0][0] == None:
				statistic_price = statistic_price + str(0) + " Kč \n"
			else:
				statistic_price = statistic_price + str(int(data[0][0])) + " Kč \n"
			statistic_count = statistic_count + str(data[0][1]) + "\n"
			statistic_sum_count = statistic_sum_count + data[0][1]
		self.label48.set_text(statistic_price)
		self.label23.set_text(statistic_count)
		sum_for_date = self.db.get_date_sum(date1,date2,item_name="",purchase_name="",category="")  
		if sum_for_date[0][0] == None:
			self.label63.set_text(str(0) + " Kč ")
		else:
			self.label63.set_text(str(int(sum_for_date[0][0])) + " Kč ")
		self.label22.set_text(str(statistic_sum_count))
		del statistic_price, statistic_count, statistic_sum_count
		del category, data, sum_for_date

	def polozka_edited(self, widget, path, text):
		self.liststore[path][0] = text
	def mnozstvi_edited(self, widget, path, text):
		self.liststore[path][1] = text
	def jednotka_edited(self, widget, path, text):
		self.liststore[path][2] = text
	def cena_edited(self, widget, path, text):
		self.liststore[path][3] = text
	def potrebnost_edited(self, widget, path, text):
		self.liststore[path][4] = text
	def ucel_edited(self, widget, path, text):
		self.liststore[path][5] = text

	def get_date(self, old):
		#print("clicked date")
		dialog = Calendar(self.window)
		if dialog.response == Gtk.ResponseType.OK or dialog.response == -1:
			#print(type(Gtk.ResponseType.OK))
			#print(type(dialog.response))
			#pprint(vars(dialog))
			date = dialog.get_date()
			day = date[2]
			month = date[1]
			year = date[0]
			month = month + 1
			if month >= 10:
				month = month
			else:
				month = '0'+str(month)
			if day >= 10:
				day = day
			else:
				day = '0'+str(day)
			return str(year)+'-'+str(month)+'-'+str(day)
		else:
			#print("aaa")
			#pprint(vars(dialog))
			return old

	def _key_press_event(self,widget,event):
		keyname = Gdk.keyval_name(event.keyval)
		ctrl = event.state & Gdk.ModifierType.CONTROL_MASK
		if ctrl:
			if keyname == 'BackSpace':
				if self.fullscreen == False:
					self.fullscreen = True
					self.window.set_resizable(True)
				else:
					self.fullscreen = False
					self.window.set_resizable(False)
			elif keyname == 'q':
				Gtk.main_quit()