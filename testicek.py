#!/usr/bin/python


try:
    import gi
except ImportError:
    print("Requires pygobject to be installed.")

try:
    gi.require_version("Gtk", "3.0")
except ValueError:
    print("Requires gtk3 development files to be installed.")
except AttributeError:
    print("pygobject version too old.")

try:
    from gi.repository import Gtk, Gdk, GObject
except (ImportError, RuntimeError):
    print("Requires pygobject to be installed.")

#from gi.repository import Gtk, Gdk, GObject
from windowManager import RendererWindow
import sys

def main():
    win = RendererWindow()
    Gtk.main()

if __name__ == "__main__":
    sys.exit(main())
