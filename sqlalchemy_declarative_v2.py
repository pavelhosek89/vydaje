#!/usr/bin/python

from sqlalchemy import Boolean, Column, Date, Float, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
 
Base = declarative_base()

class Account(Base):
	__tablename__ = 'account'
	# Here we define columns for the table person
	# Notice that each column is also a normal Python instance attribute.
	id = Column(Integer, primary_key=True)
	name = Column(String(50), nullable=False)

	def __repr__(self):
		return "<Account(id='%s', name='%s')>" % (self.id, self.name)

class Purchase(Base):
	__tablename__ = 'purchase'
	# Here we define columns for the table person
	# Notice that each column is also a normal Python instance attribute.
	id = Column(Integer, primary_key=True)
	date = Column(Date, nullable=False)
	name = Column(String(250))
	comment = Column(String(250))
	receipt = Column(String(250))
	empty = Column(Boolean, default=False)
	account_id = Column(Integer, ForeignKey('account.id'))
	account = relationship(Account)

	def __repr__(self):
		return "<Purchase(id='%s', date='%s', name='%s', comment='%s', receipt='%s', empty='%s')>" % (self.id, self.date, self.name, self.comment, self.receipt, self.empty)

class Item(Base):
	__tablename__ = 'item'
	# Here we define columns for the table address.
	# Notice that each column is also a normal Python instance attribute.
	id = Column(Integer, primary_key=True)
	name = Column(String(250), nullable=False)
	amount = Column(Integer, nullable=False)
	unit = Column(String(250), nullable=False)
	price = Column(Float, nullable=False)
	usefulness = Column(String(250), nullable=False)
	category = Column(String(250), nullable=False)
	comment = Column(String(250))
	purchase_id = Column(Integer, ForeignKey('purchase.id'))
	purchase = relationship(Purchase)

	def __repr__(self):
		return "<Item(id='%s', name='%s', amount='%s', unit='%s', price='%s', usefulness='%s', category='%s', comment='%s', purchase_id='%s')>" % (
				self.id, self.name, self.amount, self.unit, self.price, self.usefulness, self.category, self.comment, self.purchase_id)

class Combobox(Base):
	__tablename__ = 'combobox'
	# Here we define columns for the table person
	# Notice that each column is also a normal Python instance attribute.
	id = Column(Integer, primary_key=True)
	name = Column(String(250), nullable=False)
	type = Column(String(250), nullable=False)

	def __repr__(self):
		return "<Combobox(id='%s', name='%s', type='%s')>" % (self.id, self.name, self.type)

# Create an engine that stores data in the local directory's
# sqlalchemy_example.db file.
engine = create_engine('sqlite:///database.sqlite')

# Create all tables in the engine. This is equivalent to "Create Table"
# statements in raw SQL.
Base.metadata.create_all(engine)