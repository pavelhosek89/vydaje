#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, csv, sqlite3

def main():
	# database file input
	con = sqlite3.connect(sys.argv[1])
	cur = con.cursor()

	# CSV file input
	with open(sys.argv[2], "r") as f:
		# no header information with delimiter
		reader = csv.reader(f, delimiter=';')
		for row in reader:
			# Appends data from CSV file representing and handling of text
			to_db = [unicode(row[1], "utf8"),
					unicode(row[2], "utf8"),
					unicode(row[3], "utf8"),
					unicode(row[4], "utf8"),
					unicode(row[5], "utf8"),
					unicode(row[6], "utf8"),
					unicode(row[7], "utf8")]
			cur.execute("""INSERT INTO item (name, amount,
						unit, price,
						usefulness, category,
						purchase_id) VALUES(?, ?, ?, ?, ?, ?, ?);
						""", to_db)
			con.commit()
	con.close() # closes connection to database

if __name__=='__main__':
	main()

